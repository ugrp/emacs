;; (toggle-spotify-playback)
;; (get-spotify-user-data)
;; (list-spotify-user-playlists)
;; (get-spotify-user-liked-tracks)

(require 'url)

(defvar-local spotify-authinfo "spotify.com")

(defun read-spotify-api-key ()
  "Get the Supabase API key from the auth-sources' .authinfo files."
  (let ((credentials (auth-source-search :host spotify-authinfo :max 1 :create t)))
    (when credentials
      (let ((entry (car credentials)))
        (funcall (plist-get entry :secret))))))

(defun get-spotify (endpoint)
  "Make a GET request to the Spotify API."
  (let* ((access-token (read-spotify-api-key))
         (url-request-method "GET")
         (url-request-extra-headers
          `(("Authorization" . ,(concat "Bearer " access-token)))))
    (with-current-buffer (url-retrieve-synchronously endpoint)
      (goto-char url-http-end-of-headers)
      (delete-region (point-min) (point))
      (buffer-string))))

;; (defun get-spotify (endpoint)
;;   "Make a GET request to the Spotify API."
;;   (let* ((access-token (read-spotify-api-key))
;;          (command (concat "curl -X GET -H 'Authorization: Bearer " access-token "' -s " endpoint)))
;;     (shell-command-to-string command)))

(defun toggle-spotify-playback ()
  "Toggle Spotify playback on or off."
  (interactive)
  (let* ((access-token (read-spotify-api-key))
         (current-status-endpoint "https://api.spotify.com/v1/me/player")
         (pause-endpoint "https://api.spotify.com/v1/me/player/pause")
         (current-status (get-spotify current-status-endpoint))
         (is-playing (string-match "is_playing\":\\s-*true" current-status)))
    (if is-playing
        (shell-command-to-string
         (concat "curl -X PUT -H 'Authorization: Bearer " access-token "' -s " pause-endpoint))
				(shell-command-to-string
				 (concat "curl -X PUT -H 'Authorization: Bearer " access-token "' -s https://api.spotify.com/v1/me/player/play")))))

(defun get-spotify-user-data ()
  "Retrieve and display Spotify user data."
  (interactive)
  (let* ((endpoint "https://api.spotify.com/v1/me")
         (response (get-spotify endpoint))
         (data (ignore-errors (json-parse-string response :object-type 'hash-table))))
    (when data
      (message "Display Name: %s" (gethash "display_name" data))
      (message "Followers: %s" (gethash "total" (gethash "followers" data)))
      (message "Profile Picture: %s" (gethash "url" (car (gethash "images" data)))))))

(defun list-spotify-user-playlists ()
  "Retrieve and list all user playlists from Spotify."
  (interactive)
  (let* ((endpoint "https://api.spotify.com/v1/me/playlists")
         (response (get-spotify endpoint))
         (data (ignore-errors (json-parse-string response :object-type 'hash-table))))
    (when data
      (let ((items (gethash "items" data)))
        (dolist (playlist items)
          (let* ((playlist-name (gethash "name" playlist))
                 (playlist-id (gethash "id" playlist)))
            (message "Playlist: %s (ID: %s)" playlist-name playlist-id)))))))

(defun get-spotify-user-liked-tracks ()
  "Retrieve and list user's liked tracks from Spotify."
  (interactive)
  (let* ((endpoint "https://api.spotify.com/v1/me/tracks")
         (response (get-spotify endpoint))
         (data (ignore-errors (json-parse-string response :object-type 'hash-table))))
    (when data
      (let ((items (gethash "items" data)))
        (dolist (item items)
          (let* ((track (gethash "track" item))
                 (track-name (gethash "name" track))
                 (artist-names (mapconcat (lambda (artist) (gethash "name" artist))
                                          (gethash "artists" track) ", ")))
            (message "Liked Track: %s - %s" track-name artist-names)))))))
