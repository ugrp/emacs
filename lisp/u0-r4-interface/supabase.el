(require 'json)

(defvar-local supabase-api-endpoint "https://myjhnqgfwqtcicnvcwcj.supabase.co")
(defvar-local supabase-authinfo "api.radio4000.com")

(defun read-supabase-api-key ()
  "Get the Supabase API key from the auth-sources' .authinfo files."
  (let ((credentials (auth-source-search :host supabase-authinfo :max 1)))
    (when credentials
      (let ((entry (car credentials)))
        (funcall (plist-get entry :secret))))))

(defun write-supabase-api-key ()
  "Write the Supabase API key to the .authinfo file."
  (let ((credentials (auth-source-search
											 :host supabase-authinfo
											 :max 1
											 :create t)))
    (when credentials
      (message (format "%s" credentials)))))

(defun build-supabase-endpoint (model filters &optional select order limit offset)
  (let* ((base-endpoint (format "/rest/v1/%s?" model))
         (query-string (when filters
                         (concat "&"
                                 (mapconcat (lambda (filter)
                                              (format "%s=%s.%s" (car filter) (cadr filter) (caddr filter)))
                                            filters
                                            "&"))))
         (select-string (when select (format "&select=%s" select)))
         (order-string (when order (format "&order=%s" order)))
         (limit-string (when limit (format "&limit=%s" limit)))
         (offset-string (when offset (format "&offset=%s" offset))))
    (concat supabase-api-endpoint base-endpoint query-string select-string order-string limit-string offset-string)))

(defun supabase-get (model &optional filters select order limit offset)
  (let* ((url-request-method "GET")
         (url-request-extra-headers `(("apikey" . ,(read-supabase-api-key))))
         (url (build-supabase-endpoint model filters select order limit offset))
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (goto-char (point-min))
      (re-search-forward "^$")
      (json-read-from-string (delete-and-extract-region (point) (point-max))))))

(defun get-channels (&optional filters select order limit offset)
  (supabase-get "channels" filters select order limit offset))

(defun get-channel (id &optional select order limit offset)
  (get-channels `(("id" "eq" ,id)) select order limit offset))

(defun get-channel-by-slug (slug &optional select order limit offset)
  (get-channels `(("slug" "eq" ,slug)) select order limit offset))

(defun get-user-channels (user-id &optional filters select order limit offset)
  (let ((all-filters (append `(("user_id" "eq" ,user-id)) filters)))
    (supabase-get "user_channel" all-filters select order limit offset)))

(defun get-tracks (&optional filters select order limit offset)
  (supabase-get "tracks" filters select order limit offset))

(defun get-track (id &optional select order limit offset)
  (get-tracks `(("id" "eq" ,id)) select order limit offset))

;; Get a channel's tracks
(defun get-channel-tracks (channel-id &optional filters select order limit offset)
  (let ((all-filters (append `(("channel_id" "eq" ,channel-id)) filters)))
    (supabase-get "channel_track" all-filters select order limit offset)))

;; get all accounts
(defun get-accounts (&optional filters select order limit offset)
  (supabase-get "accounts" filters select order limit offset))

(defun get-account (id &optional select order limit offset)
  (get-accounts `(("id" "eq" ,id)) select order limit offset))

;; Get all users
(defun get-users (&optional filters select order limit offset)
  (supabase-get "auth.users" filters select order limit offset))

;; Get a specific user by ID
(defun get-user (id &optional select order limit offset)
  (get-users `(("id" "eq" ,id)) select order limit offset))


;; Get a channel's followers
(defun get-channel-followers (channel-id &optional filters select order limit offset)
  (let ((all-filters (append `(("channel_id" "eq" ,channel-id)) filters)))
    (supabase-get "followers" all-filters select order limit offset)))


;; (get-channels)
;; (get-channel-by-slug "oskar")
;; (get-channel "7652cf7b-4258-428b-8954-058a447b001c")
;; (get-tracks)

;; (get-channel-tracks "7652cf7b-4258-428b-8954-058a447b001c")
;; (get-channel-followers "7652cf7b-4258-428b-8954-058a447b001c")

;; should not return anythingif not logged in
;; (get-accounts)
;; (get-account)


(provide 'supabase-database)
