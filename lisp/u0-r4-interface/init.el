(require 'supabase)
(require 'interface)

(defun status ()
  "Create a new r4 buffer with the status of the site. It will show the channels."
  (interactive)
  (let* ((channels (get-channels))
         (r4-buffer-name "**r4-status**")
         (entries nil))
    (seq-doseq (channel channels)
      (push (list (cdr (assoc 'id channel))
              (vector
                (format "%s" (cdr (assoc 'created_at channel)))
                (format "%s" (cdr (assoc 'updated_at channel)))
                (list (cdr (assoc 'slug channel)) :type 'r4-xref-channel 'action-data channel)
                (cdr (assoc 'name channel))
                (format "%s" (cdr (assoc 'tracksLength channel)))
                (format "%s" (or (cdr (assoc 'body channel)) ""))
                (format "%s" (cdr (assoc 'url channel)))))
        entries))
    (generate-list-interface entries r4-buffer-name 'r4-mode)))


(provide 'u0-r4-interface)
