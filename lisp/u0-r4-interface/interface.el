(define-button-type 'r4-xref-channel
  'action 'button-channel-action)

(define-derived-mode r4-mode tabulated-list-mode "r4"
  "Major mode for handling r4."
  (setq major-mode 'r4-mode)
  (setq mode-name "R4")
  (setq tabulated-list-format
    [("created" 15 t)
      ("updated" 15 t)
      ("slug" 25 t)
      ("name" 30 t)
      ("tracks" 10 sort-tracks-predicate)
      ("body" 50 t)
      ("url" 15 t)])
  (setq tabulated-list-sort-key '("created" "updated" "slug" "name" "url" "tracks"))
  (set-keymap-parent (make-sparse-keymap) tabulated-list-mode-map)
  (tabulated-list-init-header)
  (tabulated-list-print))

(defun sort-tracks-predicate (tabulated-item-a tabulated-item-b)
  "Sort predicate for comparing two tracks."
  (>
		(string-to-number (elt (cadr tabulated-item-a) 4))
		(string-to-number (elt (cadr tabulated-item-b) 4))))

(defun generate-list-interface (entries buffer-name mode)
  "Generate a list interface given entries, buffer name and mode."
  (let ((buffer (get-buffer-create buffer-name)))
    (with-current-buffer buffer
      (setq buffer-file-coding-system 'utf-8)
      (setq tabulated-list-entries entries)
      (switch-to-buffer buffer)
      (funcall mode))))

(provide 'interface)
