(defun u0-openai-interface-region-prompt (start end)
  "Do completion on region"
  (interactive "r")
  (codegpt--internal
   (read-string "@ai#region: ")
   start end))

(defun u0-openai-interface-char-prompt ()
  "Do completion at character"
  (interactive)
  (codegpt--internal
   (read-string "@ai#prompt: ")
   1 1))

(defun u0-openai-interface (start end)
  "Do completion on region or character"
  (interactive "r")
  (if (region-active-p)
      (u0-openai-interface-region-prompt start end)
	(u0-openai-interface-char-prompt)))

(provide 'u0-openai-interface)
